import 'package:flutter/material.dart';
import 'package:generic_bloc_provider/generic_bloc_provider.dart';
import 'package:quick/src/bloc/comentarios_bloc.dart';
import 'package:quick/src/bloc/registros_bloc.dart';
import 'package:quick/src/pages/detalle_page.dart';
import 'package:quick/src/pages/home_page.dart';
import 'package:quick/src/providers/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider(
        child: MaterialApp(
            title: 'Material App',
            debugShowCheckedModeBanner: false,
            initialRoute: 'home',
            routes: {
          'home': (BuildContext context) => BlocProvider(
                child: HomePage(),
                bloc: RegistrosBloc(),
              ),
          'detalles': (BuildContext context) => BlocProvider(
                child: DetallePage(),
                bloc: ComentariosBloc(),
              ),
        }));
  }
}
