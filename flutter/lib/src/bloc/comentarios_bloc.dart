import 'dart:async';

import 'package:generic_bloc_provider/generic_bloc_provider.dart';
import 'package:quick/src/models/comentarios_model.dart';
import 'package:quick/src/providers/comentarios_provider.dart';

class ComentariosBloc extends Bloc {
  final _comentariosController = StreamController<List<Comentario>>.broadcast();

  // Recuperar los datos del Stream
  Stream<List<Comentario>> get comentariosStream =>
      _comentariosController.stream;

  // Insertar valores al Stream
  Function(List<Comentario>) get changeComentarios =>
      _comentariosController.sink.add;

  Future<void> getComentarios(String postId) async {
    List<Comentario> comentarios =
        await ComentarioProvider().getComentarios(postId);

    try {
      changeComentarios(comentarios);
    } catch (e) {}
  }

  dispose() {
    _comentariosController?.close();
  }
}
