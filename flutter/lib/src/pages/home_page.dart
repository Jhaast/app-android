import 'package:flutter/material.dart';
import 'package:generic_bloc_provider/generic_bloc_provider.dart';
import 'package:quick/src/bloc/registros_bloc.dart';
import 'package:quick/src/models/registros_model.dart';

class HomePage extends StatelessWidget {
  RegistrosBloc registros;

  @override
  Widget build(BuildContext context) {
    registros = BlocProvider.of(context);
    registros.getRegistros();
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: StreamBuilder(
        stream: registros.registrosStream,
        initialData: null,
        builder:
            (BuildContext context, AsyncSnapshot<List<Registro>> snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (context, i) =>
                    _registros(context, snapshot.data[i]));
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }

  Widget _registros(BuildContext context, Registro data) {
    return InkWell(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        child: Row(
          children: <Widget>[
            SizedBox(width: 20.0),
            Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(data.userId,
                      style: Theme.of(context).textTheme.headline6),
                  Text(
                    data.title,
                    style: TextStyle(color: Colors.blueAccent),
                  ),
                  Text(data.body, style: Theme.of(context).textTheme.subtitle2),
                  Divider()
                ],
              ),
            )
          ],
        ),
      ),
      onTap: () {
        Navigator.pushNamed(context, 'detalles', arguments: data);
      },
    );
  }

  void dispose() {
    registros.dispose();
  }
}
