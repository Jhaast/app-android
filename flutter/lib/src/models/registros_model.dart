
class Registros {
  List<Registro> items = new List();

  Registros.fromJsonList( List<dynamic> jsonList  ) {

    if ( jsonList == null ) return;

    for ( var item in jsonList  ) {
      final registros = new Registro.fromJsonMap(item);
      items.add( registros );
    }
  }
}


class Registro {
  String id;
  String userId;
  String title;
  String body;

  Registro({
    this.id,
    this.userId,
    this.title,
    this.body
  });
    
  Registro.fromJsonMap( Map<String, dynamic> json ) {

    id        = json['id'];
    userId    = json['user_id'];
    title     = json['title'];
    body      = json['body'];
  }  
}
